#!/bin/bash

# An auxiliary script to rebuild a zulip archive.
# Assumes that a zulip-archive clone is present and correctly configured.

set -e

ZULIPRC=$1
FINAL_DIRECTORY=$2

# This script assumes that zulip-archive contains a configured clone
# of https://github.com/zulip/zulip-archive.
# In particular, with the default settings.py, we recommend defining the following variables:
#   - PROD_ARCHIVE=1
#   - SITE_URL
#   - HTML_ROOT
#   - ZULIP_ICON_URL
#   - JSON_DIRECTORY
#   - HTML_DIRECTORY

# Copy zuliprc to the zulip-archive repository (the archive commands looks for it).
cp $ZULIPRC zulip-archive/zuliprc

echo
echo "# Fetch the chat data (populates the cache)."
if [ ! -d $JSON_DIRECTORY ]
then
    echo "$JSON_DIRECTORY not found: full data fetch."
    mkdir -p $JSON_DIRECTORY
    (cd zulip-archive; python3 archive.py -t || exit 2)
else
    echo "$JSON_DIRECTORY already exists: incremental data fetch."
    (cd zulip-archive; python3 archive.py -i || exit 2)
fi

echo
echo "# Build the HTML pages."
(cd zulip-archive; mkdir -p $HTML_DIRECTORY $HTML_DIRECTORY/_layouts)
(cd zulip-archive; python3 archive.py -b || exit 3)

# Remove zuliprc from zulip-archive, to make sure
# it is not included in the generated website.
rm zulip-archive/zuliprc

if [ -f style.css ]
then
    echo "Found a style.css page, copying all *.css files to the website directory."
    cp *.css $HTML_DIRECTORY/
fi

echo
echo "# Copy the built website to the destination directory."
cp -r $HTML_DIRECTORY/* $FINAL_DIRECTORY/
